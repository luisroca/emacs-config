(require 'package)
(add-to-list 'package-archives '("melpa-stable" . "https://stable.melpa.org/packages/") t)
(add-to-list 'package-archives '("gnu" . "https://elpa.gnu.org/packages/") t)
(add-to-list 'package-archives '("org" . "https://orgmode.org/elpa/") t)
(package-initialize)

(unless (package-installed-p 'use-package)
    (package-refresh-contents)
    (package-install 'use-package))

(eval-when-compile
  (require 'use-package))

(use-package use-package
  :config
  (setq use-package-always-ensure t))

(use-package delight)

(menu-bar-mode -1)
(when (fboundp 'tool-bar-mode)
  (tool-bar-mode -1))
(when (fboundp 'scroll-bar-mode)
  (scroll-bar-mode -1))
(when (fboundp 'horizontal-scroll-bar-mode)
  (horizontal-scroll-bar-mode -1))

(set-frame-parameter nil 'fullscreen 'fullboth)

(fringe-mode 18)

    ;; (require 'powerline)
    ;; (powerline-default-theme)

    (use-package golden-ratio
	      :init
	      (golden-ratio-mode 1)
	      :delight)

    (use-package neotree
	    :delight)

    (use-package mosey
	    ;; slight keyboard movement enhancement
	    :init
	    (mosey 1)
	    :bind (
	    ("C-a" . mosey-backward-bounce)
	    ("C-e" . mosey-forward-bounce))
	    :delight )	

    (use-package beacon
	    :init
	    (beacon-mode 1)
	    :pin gnu
	    :delight)

(autoload 'zap-up-to-char "misc"
  "Kill up to, but not including ARGth occurrence of CHAR." t)

(require 'uniquify)
(setq uniquify-buffer-name-style 'post-forward)
      ;; Display buffer as: 'File|location_in_directory' or 'File|top_dir/mid_dir/lower_dir'

(require 'delight)
      ;; Used here to mostly eliminate a cluttered mode line.

(require 'bind-key)
      ;; Simple, clean personal keybindings.
      ;; Included keybinding option with use-package to set primary commands for packages.
      ;; e.g.; hippie-expand:

      (bind-key "M-/" 'hippie-expand)

(require 'package)

(require 'saveplace)
(setq-default save-place t)
      ;; Point returns to last location within last file from previous session.

(setq-default indent-tabs-mode nil)
(setq-default tab-width 4)   ;; change this to 2 if that is the width
(setq indent-line-function 'insert-tab)
(c-set-offset 'case-label '+)	;; switch statement indentation

(use-package counsel
	:bind
	("M-x" . counsel-M-x)
	("C-x C-f" . counsel-find-file)
	("<f1> f" . counsel-describe-function)
	("<f1> v" . counsel-describe-variable)
	("<f1> l" . counsel-find-library)
	("<f2> i" . counsel-info-lookup-symbol)
	("<f2> u" . counsel-unicode-char)
	("C-c g" . counsel-git)
	("C-c j" . counsel-git-grep)
	("C-c k" . counsel-ag)
	("C-x l" . counsel-locate)
	("C-S-o" . counsel-rhythmbox)
	:init
	(counsel-mode 1)
	:delight)

  (use-package ivy
	:bind
	("C-c C-r" . ivy-resume)
	:init
        (ivy-mode 1)
	:delight)	

;;  (use-package ivy-hydra)

(use-package swiper
       :bind
       ("C-s" . swiper)
       :delight)

(use-package company
      :init
      (global-company-mode 1)
      :delight)

 (defun company-jedi-setup ()
   (add-to-list 'company-backends 'company-jedi))
 (add-hook 'python-mode-hook 'company-jedi-setup)

(use-package find-file-in-project)

(use-package which-key
      :init
      (which-key-mode 1)
      :delight)

(use-package hydra
      :delight)

(use-package magit)

(use-package flycheck
	      :config
	      (global-flycheck-mode 1))

      (use-package flyspell
	      :config
	      (flyspell-mode 1))

(use-package flycheck
  :delight)

(use-package flyspell
  :delight)

(use-package undo-tree
	      :init
	      (global-undo-tree-mode 1))

(use-package undo-tree
  :delight)

(use-package yasnippet
	:init
	(yas-global-mode 1)
	:delight)

(use-package smartparens
	      :config
	      (smartparens-global-mode 1))

(use-package smartparens
	      :delight)

(use-package rainbow-delimiters
  :config
  (rainbow-delimiters-mode 1))

(use-package rainbow-delimiters
  :delight)

(use-package open-junk-file)
(use-package immortal-scratch)

(use-package all-the-icons)
(use-package all-the-icons-ivy)

(use-package solarized-theme)
(load-theme 'solarized-light t)

(use-package font-utils)
(use-package dynamic-fonts
	:init
	(dynamic-fonts-setup))

(set-face-attribute 'default nil :height 140)
(set-frame-font "Nimbus Mono PS" nil t)

(use-package elpy
    :init )
;;    (setq python-shell-interpreter "jupyter"
;;          python-shell-interpreter-args "console --simple-prompt")

    (setq python-shell-interpreter "ipython"
          python-shell-interpreter-args "-i --simple-prompt")

    (when (require 'flycheck nil t)
    (setq elpy-modules (delq 'elpy-module-flymake elpy-modules))
    (add-hook 'elpy-mode-hook 'flycheck-mode))

    (use-package py-autopep8)

    (require 'py-autopep8)
    (add-hook 'elpy-mode-hook 'py-autopep8-enable-on-save)

    (use-package ein)

(load (expand-file-name "quicklisp/slime-helper.el"))
(setq inferior-lisp-program "/usr/bin/sbcl") ; your Lisp system

(use-package web-mode)
(use-package htmlize)

(use-package markdown-mode
  :ensure t
  :commands (markdown-mode gfm-mode)
  :mode (("README\\.md\\'" . gfm-mode)
   ("\\.md\\'" . markdown-mode)
   ("\\.markdown\\'" . markdown-mode))
  :init (setq markdown-command "multimarkdown"))

(use-package yaml-mode)

;; org-plus-contrib /contrib git repo
;;;; (use-package org-plus-contrib)

;; Set directory for org-mode && org-capture
(custom-set-variables
 '(org-directory "~/Documents/org"))

;; Set up capture files and keybindings

(setq org-capture-templates
   `(("t" "Todo" entry (file+headline ,(concat org-directory "tasks.org") "Inbox")
      "* TODO %?\n  %i\n  %a")

     ("j" "Journal" entry (file+datetree ,(concat org-directory "journal.org") "Entries")
      "* %?\nEntered on %?\n  %i\n  %a")

     ("b" "Bug" entry (file+datetree ,(concat org-directory "bugs.org") "Bugs to File")
      "* %?\n  %i\n  %a")
    ))

;; Org-mode Keybindings
(bind-key "C-c c" 'org-capture)
(bind-key "C-c a" 'org-agenda)

;; ox-rst reStructuredText
  (use-package ox-rst)

(use-package camcorder)

;;shortcut functions
(defun bjm/elfeed-show-all ()
  (interactive)
  (bookmark-maybe-load-default-file)
  (bookmark-jump "elfeed-all"))
(defun bjm/elfeed-show-fedora ()
  (interactive)
  (bookmark-maybe-load-default-file)
  (bookmark-jump "elfeed-fedora"))
(defun bjm/elfeed-show-emacs ()
  (interactive)
  (bookmark-maybe-load-default-file)
  (bookmark-jump "elfeed-emacs"))
(defun bjm/elfeed-show-daily ()
  (interactive)
  (bookmark-maybe-load-default-file)
  (bookmark-jump "elfeed-daily"))

;;functions to support syncing .elfeed between machines
;;makes sure elfeed reads index from disk before launching
(defun bjm/elfeed-load-db-and-open ()
  "Wrapper to load the elfeed db from disk before opening"
  (interactive)
  (elfeed-db-load)
  (elfeed)
  (elfeed-search-update--force))

;;write to disk when quiting
(defun bjm/elfeed-save-db-and-bury ()
  "Wrapper to save the elfeed db to disk before burying buffer"
  (interactive)
  (elfeed-db-save)
  (quit-window))

(use-package elfeed
:ensure t
:bind (:map elfeed-search-mode-map
            ("A" . bjm/elfeed-show-all)
            ("E" . bjm/elfeed-show-emacs)
            ("D" . bjm/elfeed-show-daily)
            ("q" . bjm/elfeed-save-db-and-bury)))

(provide 'config)
;;;
