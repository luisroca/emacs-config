;;; -*- no-byte-compile: t -*-
(define-package "camcorder" "20160404.2134" "Record screencasts in gif or other formats." '((emacs "24") (names "20150000") (cl-lib "0.5")) :commit "b13d939990e6709492efefc0945798adc1c0fcb9" :url "http://github.com/Bruce-Connor/camcorder.el" :keywords '("multimedia" "screencast"))
