;;; -*- no-byte-compile: t -*-
(define-package "mosey" "20161015.319" "Mosey around your buffers" '((emacs "24.4")) :commit "eb0ae6ec10f1d9828a2665476f6d6811df9b0bfa" :url "http://github.com/alphapapa/mosey.el" :keywords '("convenience"))
