(package-initialize)

(org-babel-load-file "~/.emacs.d/config.org")
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(custom-safe-themes
   (quote
    ("a8245b7cc985a0610d71f9852e9f2767ad1b852c2bdea6f4aadc12cce9c4d6d0" "8aebf25556399b58091e533e455dd50a6a9cba958cc4ebb0aab175863c25b9a4" "d677ef584c6dfc0697901a44b885cc18e206f05114c8a3b7fde674fce6180879" "3c83b3676d796422704082049fc38b6966bcad960f896669dfc21a7a37a748fa" "c74e83f8aa4c78a121b52146eadb792c9facc5b1f02c917e3dbb454fca931223" "a27c00821ccfd5a78b01e4f35dc056706dd9ede09a8b90c6955ae6a390eb1c1e" default)))
 '(elfeed-feeds
   (quote
    ("https://feeds.feedburner.com/brainpickings/rss" "https://www.oglaf.com/feeds/rss/" "https://feeds.feedburner.com/buttersafe" "https://www.smbc-comics.com/rss.php" "https://www.npr.org/rss/rss.php?id=327351768" "https://www.npr.org/rss/rss.php?id=139941248" "https://www.npr.org/rss/rss.php?id=1008" "https://ask.fedoraproject.org/en/feeds/rss/" "https://fedoramagazine.org/feed/" "https://blog.mozilla.org/opendesign/feed/" "https://hacks.mozilla.org/feed/" "https://blog.mozilla.org/?s=rss" "https://stackexchange.com/feeds/questions" "http://sachachua.com/blog/feed/" "http://pragmaticemacs.com/feed/" "http://www.wilfred.me.uk/rss.xml" "http://fedoraplanet.org/rss20.xml" "https://www.xkcd.com/rss.xml" "https://blog.mozilla.org/" "http://fedoraplanet.org/" "http://www.planet.emacsen.org/" "http://planetpython.org/rss20.xml" "https://planet.lisp.org/rss20.xml" "https://github.com/fedora-infra/bodhi/issues?q=is%3Aopen+is%3Aissue" "https://github.com/rocaluis.private.atom?token=AOM0JEsyvtKwZeFC34gfp6yDHDWLdzfkks640V3YwA==")))
 '(org-agenda-files
   (quote
    ("~/Documents/orgtasks.org" "~/Documents/orgjournal.org")))
 '(org-directory "~/Documents")
 '(package-selected-packages
   (quote
    (markdown-mode smart-mode-line yasnippet which-key use-package undo-tree solarized-theme smartparens rainbow-delimiters open-junk-file neotree mosey magit hydra golden-ratio flycheck find-file-in-project dynamic-fonts delight counsel company beacon all-the-icons-ivy)))
 '(safe-local-variable-values (quote ((js2-basic-offset . 4))))
 '(send-mail-function (quote mailclient-send-it)))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
